#!/bin/bash
#
flagh="false"
flage="false"
r=`ls -ld "$1" 2>/dev/null`
if [ $? -eq 0 ]; then
	# echo "OK argv 1 exist"
	if echo $r | grep "^d" &> /dev/null; then
		# echo "OK it directory"
		flage="true"
		name=`basename "$1"`
		if [ "." == ${name:0:1} ] &> /dev/null; then
			# echo "OK it hidden"
			flagh="true"
		fi
	fi
# else
# 	echo "bad"
fi
# echo $r
echo -e '<?xml version="1.0" encoding="UTF-8"?>'
echo -e '<directory path="'$1'">'
echo -e '\t<exist>'$flage'</exist>'
echo -e '\t<hidden>'$flagh'</hidden>'
echo -e '</directory>'
