from pathlib import Path
from sys import argv
from xml.dom import minidom

def create_tag(name: str=None, text: str=None, attributes: dict=None, *, cdata: bool=False):
	doc = minidom.Document()

	if name is None:
		return doc

	tag = doc.createElement(name)

	if text is not None:
		if cdata is True:
			tag.appendChild(doc.createCDATASection(text))
		else:
			tag.appendChild(doc.createTextNode(text))

	if attributes is not None:
		for k, v in attributes.items():
			tag.setAttribute(k, str(v))

	return tag
		
doc = create_tag()
root = create_tag('directory')
doc.appendChild(root)
    
try:
	fexist=False
	fhidden=False
	lastHiddenStr=''
	p = Path(argv[1])
	if p.is_dir():
		fexist=True
		for d in reversed(p.resolve().parts):
			if d[0]=='.':
				lastHiddenStr='last hidden:'+ d
				fhidden=True
	leaf = create_tag(p.resolve().parts[-1], lastHiddenStr, {'exist': fexist, 'hidden': fhidden})
	root.appendChild(leaf)
except Exception as e:
	print('bad')
	pass
xml_str = doc.toprettyxml(indent="  ")
print(xml_str)